

<?php 

require ('creditential.php');

    function get_exist(array $clefs){
        foreach($clefs as $clef){
            if(!isset($_GET[$clef]) || $_GET[$clef] == ''){
                return false; 
            }
        }
        return true; 
    }


    if(get_exist(['nom', 'prénom','nom_rue','num_rue','cp','ville','email'])){
       
        try {
            $bdh = new PDO('mysql:host=localhost;dbname=php_bdd',$user,$password);
            $stmt = $bdh->prepare("INSERT INTO utilisateurs (nom,prenom,nom_rue,num_rue,cp,ville,email) VALUES (:nom, :prenom, :nom_rue,:num_rue,:cp,:ville,:email)");
            $stmt->bindParam(':nom', $_GET['nom']);        
            $stmt->bindParam(':prenom', $_GET['prénom'] );
            $stmt->bindParam(':nom_rue', $_GET['nom_rue']);        
            $stmt->bindParam(':num_rue', $_GET['num_rue'] );
            $stmt->bindParam(':cp', $_GET['cp']);        
            $stmt->bindParam(':ville', $_GET['ville'] );
            $stmt->bindParam(':email', $_GET['email']);        
            $stmt->execute();  
        }
        catch(exception $e){
            var_dump($e);
        }


    } 

 ?> 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>
    
    <a href="ajout.php">retourner au formulaire</a>
</body>
</html>


